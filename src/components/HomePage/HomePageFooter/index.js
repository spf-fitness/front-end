import ContactMe from './ContactMe';
import './index.css'

const Services = () => {
    return (
        <ul>
            <li>
                Personal Training
            </li>
            <li>
                Weight Management Support
            </li>
            <li>
                Group Training
            </li>
        </ul>
    );
}
const Address = () => <p>2 East Main Street<br />Mascoutah, Illinois 62258</p>;
const Map = () => <iframe 
    className="map" 
    title="map" 
    src="https://www.google.com/maps/embed/v1/place?q=2+E+Main+St.%0AMascoutah%2C+IL+62258&amp;key=AIzaSyD09zQ9PNDNNy9TadMuzRV_UsPUoWKntt8" 
    aria-hidden="true"></iframe>


const FooterSection = (props) => {
    return (
        <div className='footer-section'>
            <h1>{props.title}</h1>
            {props.title==="My Services"? <Services />: ''}
            {props.title==="Contact Me!"? <ContactMe />: ''}
            {props.title==="Location"? <Address />: ''}
            {props.title==="Map"? <Map />: ''}
        </div>
    )
}


const HomePageFooter = () => {
    return (
        <div className='home-footer'>
            <FooterSection title="My Services"/>
            <FooterSection title="Contact Me!"/>
            <FooterSection title="Location"/>
            <FooterSection title="Map"/>
        </div>
    );
};

export default HomePageFooter;