import construction from './construction.png'; 

import './index.css';

const UnderConstruction = () => {
    return (
        
        <img src={construction} 
             alt="Under Construction" 
             className="bg"
        />
    )
}

export default UnderConstruction;