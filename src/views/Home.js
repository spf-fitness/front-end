import HomePage from '../components/HomePage';

const Home = () => {
    return (
        <section>
            <HomePage />
        </section>
    );
};

export default Home;